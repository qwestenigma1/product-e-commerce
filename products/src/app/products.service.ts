import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {
  products = [];
  cart = [];

  constructor(private http: HttpClient) { }

  fetchProducts() {
    var url = 'http://localhost:9000/api/products';
    return this.http.get<any[]>(url);
  }

  fetchOrders() {
    var url = 'http://localhost:9000/api/orders';
    return this.http.get<any[]>(url);
  }

  addProductToCart(id) {
    var filteredProducts = this.findProduct(id);
    if (filteredProducts.length > 0) {
      if (this.findProductInCart(id).length > 0) {
        this.removeFromCart(id);
      } else {
        this.cart.push(filteredProducts[0]);
      }
    }

  }

  removeFromCart(id) {
    if (this.findProductInCart(id).length > 0) {
      var product = this.findProductInCart(id)[0];
      var index = this.cart.indexOf(product);
      this.cart.splice(index, 1);
    }
  }

  clearCart() {
    this.cart = [];
  }

  findProduct(id) {
    return this.products.filter(product => (product._id === id));
  }

  findProductInCart(id) {
    return this.cart.filter(product => (product._id === id));
  }

  getCart() {
    return this.cart;
  }

  checkout(data) {
    return this.http.post('http://localhost:9000/api/checkout', data);
  }
}
